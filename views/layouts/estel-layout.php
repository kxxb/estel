<?php

/* @var $this \yii\web\View */
/* @var $content string */


use app\assets\EstelAsset;

EstelAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Estel</title>
    
    <?php $this->head() ?>
    
    
</head>
<body>
<?php $this->beginBody() ?>
    <script type="text/javascript" src="/estel-app/estel-app.js"></script>
     <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
