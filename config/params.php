<?php

$ENV =  APP_ENV;

if( $ENV == ENV_PRODUCTION)
{
    define('BASE_URL', '@web/assets/');
    define('DATA_DIR', dirname(__FILE__).'/../../data');
    define('DATA_DIR_ALL', dirname(__FILE__).'/../../dataall');
    $DBCONFIG = parse_ini_file(dirname(__FILE__).'/../../conf/database');
}
else if( $ENV == 'estel.bestmakers.ru')
{
    define('BASE_URL', '@web/data/');
    define('DATA_DIR', dirname(__FILE__).'/../../../data');
    define('UPLOAD_DIR', dirname(__FILE__).'/../../../data/uploads/');
    define('DATA_DIR_ALL', dirname(__FILE__).'/../../../dataall');
    $DBCONFIG = parse_ini_file(dirname(__FILE__).'/../../../conf/database');
}
else if( $ENV == 'estel.herokuapp.com')
{
    define('BASE_URL', '@web/data/');
    define('DATA_DIR', dirname(__FILE__).'/../../../data');
    define('UPLOAD_DIR', dirname(__FILE__).'/../../../data/uploads/');
    define('DATA_DIR_ALL', dirname(__FILE__).'/../../../dataall');
    $DBCONFIG = parse_ini_file(dirname(__FILE__).'/../../../conf/database');
}

else if( $ENV == 'estel.dev')
{
    define('BASE_URL', '@web/assets/');
    
    define('UPLOAD_DIR', dirname(__FILE__).'/../web/data/uploads/');
    define('DATA_DIR', dirname(__FILE__).'/../web/assets/');
    //define('DATA_DIR', dirname(__FILE__).'/../../data');
    define('DATA_DIR_ALL', dirname(__FILE__).'/../../dataall');
    $DBCONFIG = parse_ini_file(dirname(__FILE__).'/database');
    
}

$runtime = DATA_DIR.'/runtime/';


return [
    'adminEmail' => 'kxxb@yandex.com',
   
    
];
