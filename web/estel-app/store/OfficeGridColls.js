Ext.define('EST.store.OfficeGridColls', {
    extend: 'Ext.data.Store',
    model: 'EST.model.grid.Office',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: '/site/grid-colls',
        reader: {
            type: 'json',
          //  root: 'officeColls',
            successProperty: 'success'
        }
    }
});