Ext.define('EST.view.Housing.ReconfigureGrid', {
    extend: 'Ext.container.Container',
   
    requires: [
        'Ext.grid.*',
        //'Ext.layout.container.HBox',
        //'Ext.layout.container.VBox',
        'EST.model.grid.Office',
        'EST.model.grid.Employee',
        'EST.store.OfficeGridColls'
    ], 
    xtype: 'reconfigure-grid',
    
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    width: 500,
    height: 330,
    colls:[],
    
    lastNames: ['Jones', 'Smith', 'Lee', 'Wilson', 'Black', 'Williams', 'Lewis', 'Johnson', 'Foot', 'Little', 'Vee', 'Train', 'Hot', 'Mutt'],
    firstNames: ['Fred', 'Julie', 'Bill', 'Ted', 'Jack', 'John', 'Mark', 'Mike', 'Chris', 'Bob', 'Travis', 'Kelly', 'Sara'],
    cities: ['New York', 'Los Angeles', 'Chicago', 'Houston', 'Philadelphia', 'Phoenix', 'San Antonio', 'San Diego', 'Dallas', 'San Jose'],
    departments: ['Development', 'QA', 'Marketing', 'Accounting', 'Sales'],
    
    initComponent: function(){
        Ext.apply(this, {
            items: [{
                xtype: 'container',
                layout: 'hbox',
                defaultType: 'button',
                items: [{
                    itemId: 'showOffices',
                    text: 'Show Offices',
                    scope: this,
                    handler: this.onShowOfficesClick
                }, {
                    itemId: 'showEmployees',
                    margin: '0 0 0 10',
                    text: 'Show Employees',
                    scope: this,
                    handler: this.onShowEmployeesClick
                }]
            }, {
                margin: '10 0 0 0',
                xtype: 'grid',
                flex: 1,
                columns: [],
                viewConfig: {
                    emptyText: 'Click a button to show a dataset',
                    deferEmptyText: false
                }
            }]    
        });
        this.callParent();
    },
    
    onShowOfficesClick: function(){
        var grid = this.down('grid');
        
        Ext.suspendLayouts();
        grid.setTitle('Employees');
        grid.reconfigure(this.createOfficeStoreRem(), this.createOfficeColls());
        this.down('#showEmployees').enable();
        this.down('#showOffices').disable();
        Ext.resumeLayouts(true);  
    },
    
    
     
    createOfficeStoreRem: function(){
        var data = [],
           // fields = [],    
            i = 0,
            usedNames = {},
            usedCities = {};
    
       var officeModel = EST.model.grid.Office,
                fields = officeModel.prototype.fields.getRange();
    
        Ext.Ajax.request({
                scope:this,
                method:'get',
                async   : false,
                url: '/housings',
                params: {
                    id: 1
                },
                 callback: function(o, s, r) {
                      data = eval(r.responseText);
                      
                      console.log(Ext.Ajax.defaultHeaders);
                },

            });
            
        Ext.Ajax.request({
                scope:this,
                async   : false,
                url: '/model-fields',
                params: {
                    id: 1
                },
                 callback: function(o, s, r) {
                      fields = eval(r.responseText);
                },

            });
            
             //var officeModel = me.getUserModel(),
            
        
           //  var fields = ['city', 'totalEmployees','manager','manager2'];   
               officeModel.setFields(fields); 
         //    data = [{"city":"Tashkent","totalEmployees":13,"manager":"Ivanov"}];   
                
//        for (; i < 7; ++i) {
//            data.push({
//                city: this.getUniqueCity(usedCities),
//                manager: this.getUniqueName(usedNames).join(' '),
//                totalEmployees: Ext.Number.randomInt(10, 25)
//            });
//        }
        return new Ext.data.Store({
            //model: EST.model.grid.Office,
            model: officeModel,
            data: data
        });
    },
    createOfficeColls: function(){
      
           // this.getOfficeColls();
            
           var colls_loc = [];
           
           Ext.Ajax.request({
                scope:this,
                async   : false,
                url: 'site/grid-colls',
                params: {
                    id: 1
                },
                 callback: function(o, s, r) {
                      colls_loc = eval(r.responseText);
                },
//                success: function(response){
//                    this.colss = response.responseText;
//                   // response.responseText;
//                
//                   //return;
//                //   console.log(this.colss);
//                    // process server response here
//                }
            });
            
            console.log('createOfficeColls '+ colls_loc);
        
        //var cols = [{"flex":1,"text":"City Yii","dataIndex":"city","width":140},{"text":"Total Employees Yii","dataIndex":"totalEmployees","width":140},{"text":"Manager Yii","dataIndex":"manager","width":120}];
        return colls_loc;
        //return cols;
    },
    
    onShowEmployeesClick: function(){
        var grid = this.down('grid');
        
        Ext.suspendLayouts();
        grid.setTitle('Employees');
        grid.reconfigure(this.createEmployeeStore(), [{
            text: 'First Name',
            dataIndex: 'forename'
        }, {
            text: 'Last Name',
            dataIndex: 'surname'
        }, {
            width: 130,
            text: 'Employee No.',
            dataIndex: 'employeeNo'
        }, {
            flex: 1,
            text: 'Department',
            dataIndex: 'department'
        }]);
        this.down('#showOffices').enable();
        this.down('#showEmployees').disable();
        Ext.resumeLayouts(true);
    },
    
    createEmployeeStore: function(){
        var data = [],
            i = 0,
            usedNames = {},
            name;
                
        for (; i < 20; ++i) {
            name = this.getUniqueName(usedNames);
            data.push({
                forename: name[0],
                surname: name[1],
                employeeNo: this.getEmployeeNo(),
                department: this.getDepartment()
            });
        }
        return new Ext.data.Store({
            model: EST.model.grid.Employee,
            data: data
        });
    },
    
    
    
    createOfficeStore: function(){
        var data = [],
            i = 0,
            usedNames = {},
            usedCities = {};
                
        for (; i < 7; ++i) {
            data.push({
                city: this.getUniqueCity(usedCities),
                manager: this.getUniqueName(usedNames).join(' '),
                totalEmployees: Ext.Number.randomInt(10, 25)
            });
        }
        return new Ext.data.Store({
            model: EST.model.grid.Office,
            data: data
        });
    },
    
    // Fake data generation functions
    generateName: function(){
        var lasts = this.lastNames,
            firsts = this.firstNames,
            lastLen = lasts.length,
            firstLen = firsts.length,
            getRandomInt = Ext.Number.randomInt,
            first = firsts[getRandomInt(0, firstLen - 1)],
            last = lasts[getRandomInt(0, lastLen - 1)];
        
        return [first, last];
    },
    
    getUniqueName: function(used) {
        var name = this.generateName(),
            key = name[0] + name[1];
            
        if (used[key]) {
            return this.getUniqueName(used);
        }
    
        used[key] = true;
        return name;
    },
    
    getCity: function(){
        var cities = this.cities,
            len = cities.length;
        
        return cities[Ext.Number.randomInt(0, len - 1)];
    },
    
    getUniqueCity: function(used){
        var city = this.getCity();
        if (used[city]) {
            return this.getUniqueCity(used);
        }
    
        used[city] = true;
        return city;
    },
    
    getEmployeeNo: function() {
        var out = '',
            i = 0;
        for (; i < 6; ++i) {
            out += Ext.Number.randomInt(0, 7);
        }
        return out;
    },
    
    getDepartment: function() {
        var departments = this.departments,
            len = departments.length;
        
        return departments[Ext.Number.randomInt(0, len - 1)];
    }
});