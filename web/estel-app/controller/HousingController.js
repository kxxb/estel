Ext.define('EST.controller.HousingController', {
    extend: 'Ext.app.Controller',
    views: [
        'Housing.ListHousing',
        'Housing.ReconfigureGrid'
    ],
    stores: ['OfficeGridColls'  ],
    
    init: function() {
        this.control({
            'viewport > panel': {
                render: this.onPanelRendered
            }
        });
    },

    onPanelRendered: function() {
        console.log('The panel was rendered');
    }
});