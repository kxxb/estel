Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'EST',

    appFolder: 'estel-app',
    controllers: [
        'HousingController'
    ],

    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: {
             //   xtype: 'housinglist'
                xtype: 'reconfigure-grid'
            }
        });
    }
});