Ext.define('EST.model.grid.Employee', {
    extend: 'Ext.data.Model',
    fields: ['name', 'email']
});