<?php
header("X-Frame-Options: DENY");

date_default_timezone_set('Europe/Moscow');
// change the following paths if necessary


define('ENV_DEVELOPMENT', 'development');
define('ENV_PRODUCTION', 'production');
$ENVS = array(
		   	'estel.dev' => 'estel.dev',
		   	'estel.bestmakers.ru' => 'estel.bestmakers.ru',
		   	'estel.herokuapp.com' => 'estel.herokuapp.com',
		   	
		   	
		);

if(isset($ENVS[$_SERVER['SERVER_NAME']])){
		$ENV = $ENVS[$_SERVER['SERVER_NAME']];
                
// comment out the following two lines when deployed to production
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('YII_ENV') or define('YII_ENV', 'dev');
                
}    
else{
		$ENV = ENV_PRODUCTION;
                
}
		


// глобальное определения места
define('APP_ENV', $ENV);

// *********************************************
if( $ENV != ENV_PRODUCTION)
{
        
	error_reporting(E_ALL);
	ini_set('display_errors', true);
	
}

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
