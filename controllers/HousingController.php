<?php

namespace app\controllers;

use yii\web\Response;
use yii\data\ActiveDataProvider;
use app\models\Housing;

class HousingController extends \yii\rest\ActiveController
{
    public function behaviors()
        {
            $behaviors = parent::behaviors();
            $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
            return $behaviors;
        }
    public $modelClass = 'app\models\Housing';
   // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
//    public function actionIndex()
//    {
//        return $this->render('index');
//    }

    
    public function actionIndex()
    {
        
        return new ActiveDataProvider([
            'query' => Housing::find()->where(['id'=>4]),
        ]);
    }
}
