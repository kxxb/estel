<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\helpers\Json;
use yii\web\Response;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'estel-layout';
        return $this->render('index');
    }
    
    public function actionGridColls(){
        $cols = [];
        
        $cols[] =[
                'flex'=>1,
                'text'=>'ID',
                'dataIndex'=>'id',
             
            ];
        $cols[] =[
                'flex'=>1,
                'text'=>'Название',
                'dataIndex'=>'name',
             
            ];
        $cols[] =    [
                'text'=>'Адрес',
                'dataIndex'=>'address',
                'width'=>140
            ];
         $cols[] =   [
                'text'=>'Manager Yii',
                'dataIndex'=>'manager',
                'width'=>120
            ];
         $cols[] =   [
                'text'=>'Manager 2 Yii',
                'dataIndex'=>'manager2',
                'width'=>120
            ];
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
         return  $cols;
    }
   
    public function actionGridData(){
        $cols = [];
        
        $cols[] =[
                'city'=>'Tashkent',
                'totalEmployees'=>13,
                'manager'=>'Ivanov Иванович',
                'manager2'=>'Петров-Водкин'
             
            ];

        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
         return  $cols;
    }
    public function actionModelFields(){
        
        
        $fields =['id', 'name', 'address','manager','manager2'];
//        $cols[] =    [
//                'totalEmployees'=>13,
//                
//            ];
//         $cols[] =   [
//                'manager'=>'Ivanov',
//                
//            ];
//         $cols[] =   [
//                'text'=>'Manager 2 Yii',
//                'dataIndex'=>'manager2',
//                'width'=>120
//            ];
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
         return  $fields;
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
